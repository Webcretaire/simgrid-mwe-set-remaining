#include "simgrid/s4u.hpp"

namespace sg4 = simgrid::s4u;

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_test, "Messages specific for this s4u example");

static void master()
{
  XBT_INFO("Do regular sendto");
  sg4::Comm::sendto(sg4::Host::by_name("Tremblay"), sg4::Host::by_name("Fafard"), 100e6);
  XBT_INFO("Done");

  XBT_INFO("Wait until t=100");
  sg4::this_actor::sleep_until(100);

  XBT_INFO("Do sendto_init, set_remaining and wait");
  auto comm = sg4::Comm::sendto_init(sg4::Host::by_name("Tremblay"), sg4::Host::by_name("Fafard"));
  comm->set_remaining(100e6); // set_payload_size does have the correct behavior (results in 15s comm)
  comm->wait();
  XBT_INFO("Done, expected this comm to last 15s again but apparently it's only 3s now");
}

int main(int argc, char* argv[])
{
  sg4::Engine e(&argc, argv);
  xbt_assert(argc == 2, "Usage: %s platform_file\n", argv[0]);

  e.load_platform(argv[1]);

  sg4::Actor::create("master", e.host_by_name("Tremblay"), master);

  e.run();

  return 0;
}
