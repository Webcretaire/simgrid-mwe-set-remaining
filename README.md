# set_remaining bug

This repo showcases an MWE of `set_remaining` not affecting Comms created by `sendto_init(Host1, Host2)`

Example output is shown in out.txt : first comm takes 15s, and the second one only 3 even though they try to implement the same behavior 